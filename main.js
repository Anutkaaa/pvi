let i=3;
let idDelUser=1;
function addRow() {
  let id=++i;
  // отримуємо таблицю та тіло таблиці
  const table = document.getElementById("myTable");
  const tableBody = table.getElementsByTagName("tbody")[0];
  
  // створюємо новий рядок
  const newRow = tableBody.insertRow();
  newRow.id=`tr_${id}`;
  // створюємо комірки для нового рядка
  const cell1 = newRow.insertCell(0);
  const cell2 = newRow.insertCell(1);
  const cell3 = newRow.insertCell(2);
  const cell4 = newRow.insertCell(3);
  const cell5 = newRow.insertCell(4);
  const cell6 = newRow.insertCell(5);
  const cell7 = newRow.insertCell(6);

  // додаємо текст до комірок
  cell1.innerHTML ="<input type ='checkbox'></input>";
  cell2.innerHTML = "1";
  cell3.innerHTML = "50";
  cell4.innerHTML ="7";
  cell5.innerHTML = "1";
  cell6.innerHTML = "<div id = 'circle'></div>";
  cell7.innerHTML = `<button class='edit'>P</button> <button class='end'>E</button>`;
  cell7.querySelector(".end").onclick=function(){
    deleteRow(id);
  }
  cell1.querySelector("input").onclick=function(e){
    setColor(e,id);
  }
}
function deleteRow(id) {
  console.log(id);
  idDelUser=id;
  showDialogDelRow();
}
function deleteRow2(){
  document.getElementById("tr_"+idDelUser).remove();
  myDialogDelRow();
}
function setColor(e,id){
  document.querySelector("#myTable").querySelector(`#tr_${id}`).querySelector("div").style.background= e.target.checked? "grey":"green";
}
function setBackgroundAll(e){
  console.log(e.target.checked);
  document.querySelector("#myTable").querySelectorAll("#circle").forEach(x=>x.style.background=e.target.checked? "grey":"green");
}
function showDialog() {
  document.getElementById("myDialog").showModal();
}
function showDialogDelRow() {
  document.getElementById("myDialogDelRow").showModal();
}
function okClickAddRow() {
  addRow();
  document.getElementById("myDialog").close();
}

function cancelClick() {
  document.getElementById("myDialog").close();
}
function myDialogDelRow(){
  document.getElementById("myDialogDelRow").close();

}